#!/bin/sh

for file in $(ls -1 *.png | cut -d "." -f 1); do
	grep -x $file list.txt 2>&1 1>/dev/null ||
		echo $file >>list.txt
done

sort list.txt -o list.txt
